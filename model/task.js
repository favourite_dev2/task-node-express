class Task{
    constructor(id, description='', date='', type=''){
        this.id = id;
        this.description = description;
        this.date = date;
        this.type = type;
    }
}
module.exports = Task;