var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var connection = require('express-myconnection');
var mysql = require('mysql');

var indexRouter = require('./routes/index');
var users = require('./routes/api/users');
var deletes = require('./routes/api/deletes');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(
  connection(mysql, {
    host: 'localhost', //'localhost',
    user: 'root',
    password: '',
    port: 3306, //port mysql
    database: 'task_db'
  }, 'pool') //or single
);
app.use('/', indexRouter);
app.use('/api/users', users);
app.use('/api/deletes',deletes)
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
//add cni


module.exports = app;