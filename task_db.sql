/*
SQLyog Community v12.4.2 (64 bit)
MySQL - 10.1.33-MariaDB : Database - task_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`task_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `task_db`;

/*Table structure for table `task_table` */

DROP TABLE IF EXISTS `task_table`;

CREATE TABLE `task_table` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` varchar(100) NOT NULL,
  `task_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8;

/*Data for the table `task_table` */

insert  into `task_table`(`id`,`description`,`date`,`task_type`) values 
(281,'Grade Homework','','New Action'),
(282,'Create report','','Waiting'),
(283,'Visit Paris','','Someday/Maybe'),
(284,'Discuss project','','Talk'),
(285,'Buy Mazarati','','Future'),
(286,'Do mailing','2012-10-26','Waiting');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
