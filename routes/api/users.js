var express = require('express');
var router = express.Router();

router.post('/', function (req, res, next) {
    var input = JSON.parse(JSON.stringify(req.body));
    req.getConnection(function (err, connection) {
        var data = {
            description: input.description,
            date: input.date,
            task_type: input.task_type
        };
        connection.query("INSERT INTO task_table set ? ", data, function (err) {});
        if (err) console.log("Error inserting : %s ", err);
        req.getConnection(function (err, connection) {
            connection.query('SELECT * FROM task_table', function (erro, rows) {
                if (erro) console.log("Error Selecting : %s ", err);
                res.json(rows); //response with json 

            });
        });
    });
});


module.exports = router;