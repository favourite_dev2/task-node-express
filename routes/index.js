var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  req.getConnection(function (err, connection) {
    var query = connection.query('SELECT * FROM task_table', function (err, rows) {
      if (err) console.log("Error Selecting : %s ", err);
      //res.json(rows);//response with json 
      res.render('index', {
        title: "Task Manager",
        data: rows
      });
    });
  });
});

module.exports = router;